FROM emscripten/emsdk:3.1.31

RUN apt update
RUN DEBIAN_FRONTEND=noninteractive
RUN apt-get install -y --no-install-recommends \
    git \
    cmake \
    libssl-dev \
    libz-dev \
    libssh2-1-dev \
    openssh-server \
    pkgconf

WORKDIR /app

RUN git clone https://github.com/libgit2/libgit2.git/
RUN cd ./libgit2 && git checkout tags/v1.5.1 && cd ../

COPY package dist
COPY scripts scripts

WORKDIR /app/scripts

CMD sh build.sh && tail -f /dev/null || tail -f /dev/null
