# libgit2-wasm

A WASM compiled version of [libgit2](https://libgit2.org/) with TypeScript support using Emscripten.
